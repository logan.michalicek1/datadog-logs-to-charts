export type TServiceLogInstance = [
  service: string,
  detail: string,
  startTime: Date,
  endTime: Date
];