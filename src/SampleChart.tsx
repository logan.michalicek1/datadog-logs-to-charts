import Chart from "react-google-charts";
import { TServiceLogInstance } from "./types";

const rows: TServiceLogInstance[] = [
  [
    "Webapp Service",
    "Create and Deploy JobGroup",
    new Date("2022-11-30T01:44:58.793Z"),
    new Date("2022-11-30T01:44:58.896Z"),
  ],
  [
    "Batch Job Runner Service",
    "Launching Job",
    new Date("2022-11-30T01:44:58.909770977Z"),
    new Date("2022-11-30T01:44:59.718916195Z"),
  ],
  [
    "Batch Job Runner Service",
    "BJR Polling for Job Status",
    new Date("2022-11-30T01:44:59.718916195Z"),
    new Date("2022-11-30T01:45:24.483172069Z"),
  ],
  [
    "Data Service",
    "Preparing Job",
    new Date("2022-11-30T01:44:59.424536858Z"),
    new Date("2022-11-30T01:44:59.529411700Z"),
  ],
  [
    "Data Service",
    "Launching Kubernetes Job",
    new Date("2022-11-30T01:44:59.529411700Z"),
    new Date("2022-11-30T01:44:59.718916195Z"),
  ],
  [
    "Data Service",
    "DS Polling for Job Status",
    new Date("2022-11-30T01:45:03.478735035Z"),
    new Date("2022-11-30T01:45:23.644265892Z"),
  ],
  [
    "Data Service",
    "Ping k8s for status",
    new Date("2022-11-30T01:45:03.844389088Z"),
    new Date("2022-11-30T01:45:03.944389088Z"),
  ],
  [
    "Data Service",
    "Ping k8s for status",
    new Date("2022-11-30T01:45:23.628479783Z"),
    new Date("2022-11-30T01:45:23.728479783Z"),
  ],
  [
    "Data Service",
    "Cleaning up K8S Job",
    new Date("2022-11-30T01:45:23.644290072Z"),
    new Date("2022-11-30T01:45:23.745109354Z"),
  ],
  [
    "Data Service",
    "Pulling Job Logs",
    new Date("2022-11-30T01:45:23.646600392Z"),
    new Date("2022-11-30T01:45:23.695388228Z"),
  ],
  [
    "Data Service",
    "Deleting Secrets",
    new Date("2022-11-30T01:45:23.695409008Z"),
    new Date("2022-11-30T01:45:23.725265955Z"),
  ],
  [
    "Data Service",
    "Deleting Job",
    new Date("2022-11-30T01:45:23.725744575Z"),
    new Date("2022-11-30T01:45:23.745097904Z"),
  ],
  [
    "Data System Job",
    "Running Job",
    new Date("2022-11-30T01:45:02.450Z"),
    new Date("2022-11-30T01:45:20.997Z"),
  ],
  [
    "Batch Job Runner Service",
    "Cleanup",
    new Date("2022-11-30T01:45:25.242561636Z"),
    new Date("2022-11-30T01:45:25.277733311Z"),
  ]
];

const columns = [
  { type: "string", id: "Service" },
  { type: "string", id: "Detail" },
  { type: "datetime", id: "Start" },
  { type: "datetime", id: "End" }
];

const data = [columns, ...rows];

const options = {
  timeline: { groupByRowLabel: false }
};

function SampleChart() {
  return (
    <div className="BigChart">
      <Chart 
        chartType='Timeline'
        data={data}
        width="98%"
        height="100%"
        options={options}
      />
    </div>
  );
}

export default SampleChart;