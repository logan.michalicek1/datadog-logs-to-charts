import './App.css';
import SampleChart from './SampleChart';

function App() {
  return (
    <div className="App">
      <SampleChart />
    </div>
  );
}

export default App;


